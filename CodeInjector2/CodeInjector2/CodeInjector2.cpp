#include <windows.h>
#include <fstream>
#include <stdlib.h>
#include <tlhelp32.h>

#pragma comment(lib,"advapi32.lib")
#pragma comment(lib,"user32.lib")

typedef BOOL(WINAPI* CreatePrcssParam)(LPCTSTR, LPTSTR, LPSECURITY_ATTRIBUTES,
	LPSECURITY_ATTRIBUTES, BOOL, DWORD, LPVOID, LPCTSTR, LPVOID, LPVOID);

struct PARAMETERS{
	LPVOID CreateProcessInj;
	char lpApplicationName[50];
	char lpCommandLine[10];
	LPSECURITY_ATTRIBUTES lpProcessAttributes;
	LPSECURITY_ATTRIBUTES lpThreadAttributes;
	BOOL bInheritHandles;
	DWORD dwCreationFlags;
	LPVOID lpEnvironment;
	LPCTSTR lpCurrentDirectory;
	LPVOID lpStartupInfo;
	LPVOID lpProcessInformation;
};

int privileges();
DWORD myFunc(PARAMETERS * myparam);
DWORD Useless();    //used to calculate size of myFunc()

DWORD getPid(){
	HANDLE hsnap;
	PROCESSENTRY32 pt;
	hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	pt.dwSize = sizeof(PROCESSENTRY32);
	do{
		if (!wcscmp(pt.szExeFile, L"ConsoleApplication1.exe")){
			DWORD pid = pt.th32ProcessID;
			CloseHandle(hsnap);
			return pid;
		}
	} while (Process32Next(hsnap, &pt));
	CloseHandle(hsnap);
	return 0;
}
int main()
{
	privileges();

	_STARTUPINFOA si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	DWORD pid = getPid();

	HANDLE p;
	p = OpenProcess(PROCESS_ALL_ACCESS, false, pid);
	if (p == NULL)
	{
		printf("ERROR");
		return 1; //error
	}

	char * AppName = "C:\\Windows\\explorer.exe";
	char * CmdLine = "";

	//Writing the structure vital for CreateProcess function
	LPVOID StrtUpInfo = VirtualAllocEx(p, NULL, sizeof(si), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(p, StrtUpInfo, &si, sizeof(si), NULL);

	LPVOID PrcssInfo = VirtualAllocEx(p, NULL, sizeof(si), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(p, PrcssInfo, &pi, sizeof(pi), NULL);
	//=========================================================

	PARAMETERS data = { 0 };
	HMODULE Kernel32 = LoadLibrary(L"Kernel32.dll");
	data.CreateProcessInj = GetProcAddress(Kernel32, "CreateProcessA");
	strcpy_s(data.lpApplicationName, AppName);
	strcpy_s(data.lpCommandLine, CmdLine);
	data.lpProcessAttributes = NULL;
	data.lpThreadAttributes = NULL;
	data.bInheritHandles = FALSE;
	data.dwCreationFlags = NULL;
	data.lpEnvironment = NULL;
	data.lpCurrentDirectory = NULL;
	data.lpStartupInfo = StrtUpInfo;
	data.lpProcessInformation = PrcssInfo;

	DWORD size_myFunc = (PBYTE)Useless - (PBYTE)myFunc;  //this gets myFunc's size

	//Writing the code part of myFunc -- Instructions to be executed
	LPVOID MyFuncAddress = VirtualAllocEx(p, NULL, size_myFunc, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	WriteProcessMemory(p, MyFuncAddress, (void*)myFunc, size_myFunc, NULL);

	//Writing the data part of myFunc -- Parameters of the functios
	LPVOID DataAddress = VirtualAllocEx(p, NULL, sizeof(PARAMETERS), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(p, DataAddress, &data, sizeof(PARAMETERS), NULL);

	HANDLE thread = CreateRemoteThread(p, NULL, 0, (LPTHREAD_START_ROUTINE)MyFuncAddress, DataAddress, 0, NULL);
	if (thread != 0){
		//injection completed, not we can wait for it to end and free the memory
		WaitForSingleObject(thread, INFINITE);   //this waits until thread thread has finished
		VirtualFree(MyFuncAddress, 0, MEM_RELEASE); //free myFunc memory
		VirtualFree(DataAddress, 0, MEM_RELEASE); //free data memory
		CloseHandle(thread);
		CloseHandle(p);  //don't wait for the thread to finish, just close the handle to the process
	}
	else{
		printf("Error!");
	}
	return EXIT_SUCCESS;
}

static DWORD myFunc(PARAMETERS * myparam){

	CreatePrcssParam CreatePrcss = (CreatePrcssParam)myparam->CreateProcessInj;
	BOOL result = CreatePrcss((LPCTSTR)myparam->lpApplicationName, NULL,
		myparam->lpProcessAttributes, myparam->lpThreadAttributes,
		myparam->bInheritHandles, myparam->dwCreationFlags, myparam->lpEnvironment,
		myparam->lpCurrentDirectory, myparam->lpStartupInfo, myparam->lpProcessInformation);
	return 0;
}

static DWORD Useless(){
	return 0;
}

//this function is needed to get some extra privileges so your code will be able to work without conflicts with the system
int privileges(){
	HANDLE Token;
	TOKEN_PRIVILEGES tp;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &Token))
	{
		LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &tp.Privileges[0].Luid);
		tp.PrivilegeCount = 1;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		if (AdjustTokenPrivileges(Token, 0, &tp, sizeof(tp), NULL, NULL) == 0){
			return 1; //FAIL
		}
		else{
			return 0; //SUCCESS
		}
	}
	return 1;
}

