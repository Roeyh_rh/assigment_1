#!/bin/bash
Name[0]=""
User[0]=""
Group[0]=""
Deletion[0]=""
place=1
temp=""
function stat_snapshot() {
	for i in "$1"/*	 
	do
		i=${i// /\ }
		if [ -d "$i" ]; then
			Name[place]="$i"
			Access[place]=$(stat "$i" --format "%X" 2> err.txt)
			User[place]=$(stat "$i" --format "%U" 2> err.txt)
			Group[place]=$(stat "$i" --format "%G" 2> err.txt)
			place=$((place+1))
			stat_snapshot "$i"
		elif [ -e "$i" ]; then
			Name[place]="$i"
			Access[place]=$(stat "$i" --format "%X" 2> err.txt)
			User[place]=$(stat "$i" --format "%U" 2> err.txt)
			Group[place]=$(stat "$i" --format "%G" 2> err.txt)
			place=$((place+1))
		fi
	done
}
function search_new() {
	for i in "$1"/*	 
	do
		i=${i// /\ }
		if [ -d "$i" ]; then
			if ! [[ "${Name[@]}" =~ "${i}" ]]; then
				temp_date=$(stat "$i" --format "%x" 2> err.txt)
				Name[place]="$i"
				Access[place]=$(stat "$i" --format "%X" 2> err.txt)
				User[place]=$(stat "$i" --format "%U" 2> err.txt)
				Group[place]=$(stat "$i" --format "%G" 2> err.txt)
				echo " The file:$i Action:CREATION at:$temp_date user:${User[place]} group:${Group[place]}"
				place=$((place+1))
			fi
			search_new "$i"
		elif [ -e "$i" ]; then
			if ! [[ " ${Name[@]} " =~ " ${i} " ]]; then
				temp_date=$(stat "$i" --format "%x" 2> err.txt)
				Name[place]="$i"
				Access[place]=$(stat "$i" --format "%X" 2> err.txt)
				User[place]=$(stat "$i" --format "%U" 2> err.txt)
				Group[place]=$(stat "$i" --format "%G" 2> err.txt)
				echo " The file:$i Action:CREATION at:$temp_date user:${User[place]} group:${Group[place]}"
				place=$((place+1))
			fi
		fi
	done
}
echo "Creating snapshot of the system"
stat_snapshot "/etc"
echo "Watching"
while true
do
	search_new "/etc"

	for j in $(seq 1 $place)
	do
		temp=$(stat ${Name[j]} --format "%X" 2> err.txt)
		temp_val=${Access[j]}
		if [ "$temp" != "" ]; then
			if [ $temp -gt $temp_val ]; then
				temp_date=$(stat ${Name[j]} --format "%x" 2> err.txt)
				Access[j]=$temp
				echo " The file:${Name[j]} Action:ACCESS at:$temp_date user:${User[j]} group:${Group[j]}"
			fi
		fi
	done
	for j in $(seq 1 $place)
	do
		if [ "${Name[j]}" != "" ]; then
			if ! [ -e "${Name[j]}" ]; then 
				echo " The file:${Name[j]} Action:DELETION at: $(date +"%Y-%m-%d %H:%M:%S") user:${User[j]} group:${Group[j]}"
				Name[j]=""
			fi
		fi
	done
done 
