// Tutorial.cpp : Defines the entry point for the console application. 
// 

//We Will Be Using These. 
#include "stdafx.h" 
#include <windows.h>  
#include <tlhelp32.h>  
#include <shlwapi.h>  
#include <conio.h>  
#include <stdio.h> 
#include <io.h>
#include <winternl.h>

//Lets Just Define Some Variables 
#define WIN32_LEAN_AND_MEAN  
#define CREATE_THREAD_ACCESS (PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ 
EXTERN_C NTSTATUS NTAPI RtlAdjustPrivilege(ULONG, BOOLEAN, BOOLEAN, PBOOLEAN);


//Lets declare our function 
BOOL CreateRemoteThreadInject(DWORD ID, const char * dll);

//Let declare GetProcessId 
DWORD GetProcessId(IN PCHAR szExeName);


void printError(TCHAR* msg)
{
	DWORD eNum;
	TCHAR sysMsg[256];
	TCHAR* p;

	eNum = GetLastError();
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, eNum,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		sysMsg, 256, NULL);

	// Trim the end of the line and terminate it with a null
	p = sysMsg;
	while ((*p > 31) || (*p == 9))
		++p;
	do { *p-- = 0; } while ((p >= sysMsg) &&
		((*p == '.') || (*p < 33)));

	// Display the message
	_tprintf(TEXT("\n  WARNING: %s failed with error %d (%s)"), msg, eNum, sysMsg);
}
//We will be writing our own little function called CreateRemoteThreadInject 
BOOL CreateRemoteThreadInject(DWORD ID, const char * dll)

{
	//Declare the handle of the process. 
	HANDLE Process;

	//Declare the memory we will be allocating 
	LPVOID Memory;

	//Declare LoadLibrary 
	LPVOID LoadLibrary;

	//If there's no process ID we return false. 
	if (!ID)
	{
		return false;
	}

	//Open the process with read , write and execute priviledges 
	Process = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, ID);

	//Get the address of LoadLibraryA 
	LoadLibrary = (LPVOID)GetProcAddress(GetModuleHandle(L"kernel32.dll"), "LoadLibraryA");

	// Allocate space in the process for our DLL  
	Memory = (LPVOID)VirtualAllocEx(Process, NULL, strlen(dll) + 1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

	// Write the string name of our DLL in the memory allocated  
	WriteProcessMemory(Process, (LPVOID)Memory, dll, strlen(dll) + 1, NULL);

	// Load our DLL  
	HANDLE thread = CreateRemoteThread(Process, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibrary, (LPVOID)Memory, NULL, NULL);
	if (thread != 0){
		WaitForSingleObject(thread, 1000);
	}
	//Let the program regain control of itself 
	CloseHandle(Process);



	//Lets free the memory we are not using anymore. 
	VirtualFreeEx(Process, (LPVOID)Memory, 0, MEM_RELEASE);

	return true;
}
BOOL GetProcessList()
{
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	DWORD dwPriorityClass;
	HMODULE hMods[1024];
	DWORD cbNeeded;
	unsigned int i;
	char* dll = "C:\\Mydll.dll";
	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		printError(TEXT("CreateToolhelp32Snapshot (of processes)"));
		return(FALSE);
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
	{
		printError(TEXT("Process32First")); // show cause of failure
		CloseHandle(hProcessSnap);          // clean the snapshot object
		return(FALSE);
	}

	// Now walk the snapshot of processes, and
	// display information about each process in turn
	do
	{
		if (!wcscmp(pe32.szExeFile, L"NETSTAT.EXE")){
			_tprintf(TEXT("\n\n====================================================="));
			_tprintf(TEXT("\nPROCESS NAME:  %s"), pe32.szExeFile);
			_tprintf(TEXT("\n-------------------------------------------------------"));

			// Retrieve the priority class.
			dwPriorityClass = 0;
			hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
				PROCESS_VM_READ | PROCESS_VM_WRITE, FALSE, pe32.th32ProcessID);
			if (hProcess == NULL)
				printError(TEXT("OpenProcess"));
			else
			{
				dwPriorityClass = GetPriorityClass(hProcess);
				if (!dwPriorityClass)
					printError(TEXT("GetPriorityClass"));
				CloseHandle(hProcess);
			}
			_tprintf(TEXT("\n  Process ID        = %d"), pe32.th32ProcessID);
			_tprintf(TEXT("\n  Thread count      = %d"), pe32.cntThreads);
			_tprintf(TEXT("\n  Parent process ID = %d"), pe32.th32ParentProcessID);
			_tprintf(TEXT("\n  Priority base     = %d"), pe32.pcPriClassBase);
			if (dwPriorityClass)
				_tprintf(TEXT("\n  Priority class    = %d"), dwPriorityClass);
			if (!CreateRemoteThreadInject(pe32.th32ProcessID, dll))
			{
				//If CreateRemoteThreadInject Returned true 
				printf("Injection failed!");
			}
			else
			{
				//If CreateRemoteThreadInject Returned true 
				printf("Injection Successful!");
			}
		}
	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);
	return(TRUE);
}
//Our Application Starts Here. 
int main()
{
	BOOLEAN bl;
	if (!NT_SUCCESS(RtlAdjustPrivilege(20, TRUE, FALSE, &bl)))
	{
		printf("Unable to enable SeDebugPrivilege. Make sure you are running this program as administrator.");
		return 1;
	}
	//We will be using this neat little function written by batfitch - GetProcessId. 
	while (true){
		GetProcessList();
	}
	return 0;
}