#include<io.h>
#include<stdio.h>
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WS2tcpip.h>
#include<winsock2.h>
#include <winternl.h>
#include <cstdio>


#pragma comment(lib,"ws2_32.lib") //Winsock Library
#define STATUS_SUCCESS  ((NTSTATUS)0x00000000L)
#define SIZE 6
  
int main()
{
	WSADATA wsa;
	SOCKET s, new_socket;
	struct sockaddr_in server, client;
	int c;
	char message[1024];
	char *ip;
	printf("\nInitializing Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		return 1;
	}

	printf("Initialized.\n");

	//Create a socket
	if ((s = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}

	printf("Socket created.\n");

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(1337);

	//Bind
	if (bind(s, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
	}

	puts("Bind done");
	while (true) {
		//Listen to incoming connections
		listen(s, 100);

		//Accept and incoming connection
		puts("Waiting for incoming connections...");

		c = sizeof(struct sockaddr_in);
		new_socket = accept(s, (struct sockaddr *)&client, &c);
		if (new_socket == INVALID_SOCKET)
		{
			printf("accept failed with error code : %d", WSAGetLastError());
		}

		puts("Connection accepted");

		//Reply to client
		ip = inet_ntoa(client.sin_addr);
		sprintf_s(message, 1024, "%s <%s>", "Hello", ip);

		send(new_socket, message, strlen(message), 0);
	}
	closesocket(s);
	WSACleanup();
}